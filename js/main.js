$(document).ready(function(){
    $('.slider__wrapper').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: ".main__slider .slider__btn--left",
        nextArrow: ".main__slider .slider__btn--right",
        dots: true,
        responsive: [
            {
                breakpoint: 550,
                settings: {
                    arrows: false
                }
            }
            ]
    });
    $('.calendar__tabs-wrapper').each(function() {
        let ths = $(this);
        ths.find('.calendar__slider').not(':first').hide();
        ths.find('.calendar__tab').click(function() {
            ths.find('.calendar__tab').removeClass('active').eq($(this).index()).addClass('active');
            ths.find('.calendar__slider').hide().eq($(this).index()).fadeIn()
        }).eq(0).addClass('active');
    });
    $('.calendar__slider--scholarships .slider').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        prevArrow: ".calendar__slider--scholarships .slider__btn--left",
        nextArrow: ".calendar__slider--scholarships .slider__btn--right",
        variableWidth: true,
        responsive: [
            {
                breakpoint: 550,
                settings: {
                    arrows: false,
                    dots: true
                }
            }
        ]
    });
    $('.calendar__slider--events .slider').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        prevArrow: ".calendar__slider--events .slider__btn--left",
        nextArrow: ".calendar__slider--events .slider__btn--right",
        variableWidth: true,
        responsive: [
            {
                breakpoint: 550,
                settings: {
                    arrows: false,
                    dots: true
                }
            }
        ]
    });
    $('.calendar__slider--lectures .slider').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        prevArrow: ".calendar__slider--lectures .slider__btn--left",
        nextArrow: ".calendar__slider--lectures .slider__btn--right",
        variableWidth: true,
        responsive: [
            {
                breakpoint: 550,
                settings: {
                    arrows: false,
                    dots: true
                }
            }
        ]
    });
    $('.calendar__slider--other .slider').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        prevArrow: ".calendar__slider--other .slider__btn--left",
        nextArrow: ".calendar__slider--other .slider__btn--right",
        variableWidth: true,
        responsive: [
            {
                breakpoint: 550,
                settings: {
                    arrows: false,
                    dots: true
                }
            }
        ]
    });
});